class Product < ApplicationRecord
    
    validates :title, :description, :image_url, presence: true          #Campos no vacios
    validates :title, presence: true, uniqueness:true                   #Título unico
    validates :price, numericality: {greater_than_or_equal_to: 0.01}    #Precio válido
    
    validates :image_url, allow_blank: true, format: {                   #Url con extensión válida
        with: %r{\.(gif|jpg|png)\Z}i,
        message: 'must be a URL for GIF, JPG or PNG image.'
    }
    
end
